/**
 * resolve a module path
 * @executes ASYNC
 * @param moduleNameArg
 * @param dirNameArg
 */
export declare let resolveInDir: (moduleNameArg: string, dirNameArg?: string) => Promise<string>;
/**
 * resolve a module path in a directory
 * @executes SYNC
 * @param moduleNameArg
 * @param dirNameArg
 */
export declare let resolveInDirSync: (moduleNameArg: string, dirNameArg?: string) => string;
export declare let requireInDir: (moduleNameArg: any, dirNameArg?: string) => Promise<any>;
export declare let requireInDirSync: (moduleNameArg: any, dirNameArg?: string) => any;
