import * as plugins from './smartrequire.plugins'

// resolving

/**
 * resolve a module path
 * @executes ASYNC
 * @param moduleNameArg
 * @param dirNameArg
 */
export let resolveInDir = async (moduleNameArg: string, dirNameArg: string = process.cwd()) => {
  let done = plugins.smartq.defer<string>()
  plugins.resolve(moduleNameArg, { basedir: dirNameArg }, function (err, resolvedPath) {
    if (err) {
      done.reject(err)
      return
    }
    done.resolve(resolvedPath)
  })
  return await done.promise
}

/**
 * resolve a module path in a directory
 * @executes SYNC
 * @param moduleNameArg
 * @param dirNameArg
 */
export let resolveInDirSync = (moduleNameArg: string, dirNameArg: string = process.cwd()) => {
  return plugins.resolve.sync(moduleNameArg, { basedir: dirNameArg })
}

export let requireInDir = async (moduleNameArg, dirNameArg = process.cwd()) => {
  let resolvedPath = await resolveInDir(moduleNameArg, dirNameArg)
  let module = require(resolvedPath)
  return module
}

export let requireInDirSync = (moduleNameArg, dirNameArg = process.cwd()) => {
  let resolvedPath = resolveInDirSync(moduleNameArg, dirNameArg)
  let module = require(resolvedPath)
  return module
}
